# devops-exercise

##NOTE: The whole exercise is done on Azure Cloud on my own MCT Subscription and its CIDC pipeline is tested and running completly fine.

##Task 1 Vagrantfile

To create VM’s with vagrant, you need to create a file name Vagrantfile which in folder 1-vagrantfile in the repository. This given Vagrantfile will set up one Ubuntu VM in VirtualBox.

And to create run the below cmd on the same different
###vagrant up

##Task 2 voting application (cats vs dogs)

The voting application is in dir 2-voting-application, and it is totally containerized (web server & database) ready to deploy on Docker or kubernetes. Further the automated deployment is written in .gitlab-ci.yml as per below stages:

stages:
  - build-voting-application
  - publish-voting-application 
  - deploy-az-infra-terraform
  - deploy-voting-application

In these stages the simple explanation is that the Application get build first as Docker Image, second stage is to publish the docker image into Azure Container Registry and third stage is Infrastructure Deployment through Terraform in Which Azure kubernetes Service will deploy with best configurations and in the last stage the voting application deploy in the AKS. And whole CI/CD is checked and run within this Gitlab.

##Task 3 database encryption

The database Username & password are encrypted with base64 within AKS through the secret.yaml

##Task 4 database persist

The database is postgressql and the volume is PVC so the data volume will retain even after restart.

##Task 5 Application scaled automatically

As the Application is deploying in AKS so 3 features are applied to handle automatically scale:

Horizontal Pod Autoscaler (HPA)
Cluster Autoscaler
AKS Deployment 

##Task 6 ReadMe

This is the ReadMe file.
