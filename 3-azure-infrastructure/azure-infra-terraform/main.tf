provider "azurerm" {
  version = "~> 2.49.0" #"~> 2.2.0"
  features {}
}

provider "azurerm" {
  alias   = "hub"
  version = "~> 2.49.0" #"~> 2.2.0"
  features {}
}

terraform {
  backend "azurerm" {
    resource_group_name  = "rg-devops"
    storage_account_name = "stgdevopsdevtfstater"
    container_name       = "remotetfstatedevopsdev"
    subscription_id      = "dfd20297-fe08-4392-ba07-a011f4b1acd7"
    key                  = "terraform.tfstate"
  }
}
