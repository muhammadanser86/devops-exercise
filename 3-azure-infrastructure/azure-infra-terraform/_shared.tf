module "shared" {
  source                       = "./shared"
  location                     = local.workspace["location"]
  rg_name                      = local.workspace["rg_name"]
  subnet_common_name           = local.workspace["subnet_common_name"]
  subnet_common_address_prefix = local.workspace["subnet_common_address_prefix"]
  subnet_aks_name              = local.workspace["subnet_aks_name"]
  subnet_aks_address_prefix    = local.workspace["subnet_aks_address_prefix"]
  vnet_address_prefix          = local.workspace["vnet_address_prefix"]
  vnet_name                    = local.workspace["vnet_name"]
  tags                         = local.tags["default"]
}
