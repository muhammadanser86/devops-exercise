variable "location" {
  type        = string
  description = "location"
}

variable "rg_name" {
  type        = string
  description = "resource group name"
}

variable "subnet_aks_name_id" {
  type        = string
  description = "subnet id "
}

variable "tags" {
  type        = map(any)
  description = "Collection of the tags referenced by the Azure deployment"
}

variable "aks_name" {
  type        = string
  description = "aks_name"
}

variable "aks_dns_prefix" {
  type        = string
  description = "aks_dns_prefix"
}
variable "kubernetes_version" {
  type        = string
  description = "kubernetes_version"
}
variable "node_resource_group" {
  type        = string
  description = "node_resource_group"
}
variable "default_node_pool_name" {
  type        = string
  description = "default_node_pool_name"
}
variable "node_count" {
  type        = string
  description = "node_count"
}
variable "node_max_count" {
  type        = string
  description = "node_max_count"
}
variable "node_min_count" {
  type        = string
  description = "node_min_count"
}
variable "node_vm_size" {
  type        = string
  description = "node_vm_size"
}
variable "node_type" {
  type        = string
  description = "node_type"
}
variable "enable_auto_scaling" {
  type        = string
  description = "enable_auto_scaling"
}
variable "load_balancer_sku" {
  type        = string
  description = "load_balancer_sku"
}
variable "network_plugin" {
  type        = string
  description = "network_plugin"
}
variable "docker_bridge_cidr" {
  type        = string
  description = "docker_bridge_cidr"
}
variable "dns_service_ip" {
  type        = string
  description = "dns_service_ip"
}
variable "service_cidr" {
  type        = string
  description = "service_cidr"
}
variable "identity_type" {
  type        = string
  description = "identity_type"
}
