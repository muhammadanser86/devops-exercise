output "aks_rg_name" {
  value = azurerm_kubernetes_cluster.aks_cluster.resource_group_name
}

output "aks_name" {
  value = azurerm_kubernetes_cluster.aks_cluster.name
}
