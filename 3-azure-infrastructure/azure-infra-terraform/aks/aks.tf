resource "azurerm_kubernetes_cluster" "aks_cluster" {
  name                = var.aks_name
  location            = var.location
  resource_group_name = var.rg_name
  dns_prefix          = var.aks_dns_prefix
  kubernetes_version  = var.kubernetes_version
  node_resource_group = var.node_resource_group
  tags                = var.tags

  default_node_pool {
    name                 = var.default_node_pool_name
    node_count           = var.node_count
    max_count            = var.node_max_count
    min_count            = var.node_min_count
    vm_size              = var.node_vm_size
    vnet_subnet_id       = var.subnet_aks_name_id
    type                 = var.node_type
    orchestrator_version = var.kubernetes_version
    enable_auto_scaling  = var.enable_auto_scaling
  }

  network_profile {
    load_balancer_sku  = var.load_balancer_sku
    network_plugin     = var.network_plugin
    docker_bridge_cidr = var.docker_bridge_cidr
    dns_service_ip     = var.dns_service_ip
    service_cidr       = var.service_cidr

  }

  identity {
    type = var.identity_type
  }

}
