resource "azurerm_virtual_network" "vnet_name" {
  name                = var.vnet_name
  location            = azurerm_resource_group.rg_name.location
  resource_group_name = azurerm_resource_group.rg_name.name
  address_space       = [var.vnet_address_prefix]
  tags                = var.tags
  depends_on          = [azurerm_resource_group.rg_name]
}

resource "azurerm_subnet" "subnet_aks_name" {
  name                 = var.subnet_aks_name
  address_prefixes     = [var.subnet_aks_address_prefix]
  resource_group_name  = azurerm_resource_group.rg_name.name
  virtual_network_name = azurerm_virtual_network.vnet_name.name
  depends_on           = [azurerm_virtual_network.vnet_name]
}

resource "azurerm_subnet" "subnet_common_name" {
  name                 = var.subnet_common_name
  address_prefixes     = [var.subnet_common_address_prefix]
  resource_group_name  = azurerm_resource_group.rg_name.name
  virtual_network_name = azurerm_virtual_network.vnet_name.name
  depends_on           = [azurerm_virtual_network.vnet_name]
}
