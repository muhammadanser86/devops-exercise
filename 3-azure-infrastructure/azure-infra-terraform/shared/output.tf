output "rg_name" {
  value = azurerm_resource_group.rg_name.name
}

output "location" {
  value = azurerm_resource_group.rg_name.location
}

output "vnet_name_id" {
  value = azurerm_virtual_network.vnet_name.id
}

output "subnet_aks_name_id" {
  value = azurerm_subnet.subnet_aks_name.id
}

output "subnet_common_name_id" {
  value = azurerm_subnet.subnet_common_name.id
}
