variable "location" {
  type        = string
  description = "location"
}

variable "rg_name" {
  type        = string
  description = "resource group name"
}

variable "vnet_name" {
  type        = string
  description = "vnet_name"
}

variable "vnet_address_prefix" {
  type        = string
  description = "vnet address prefix"
}

variable "subnet_aks_name" {
  type        = string
  description = "aks subnet name"
}

variable "subnet_aks_address_prefix" {
  type        = string
  description = "aks subnet prefix"
}

variable "tags" {
  type        = map(any)
  description = "Collection of the tags referenced by the Azure deployment"
}

variable "subnet_common_name" {
  type        = string
  description = "subnet_common_name"
}

variable "subnet_common_address_prefix" {
  type        = string
  description = "subnet_common_address_prefix"
}
