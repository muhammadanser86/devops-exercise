locals {
  tags = {
    default = {
      Source  = "Terraform"
      Contact = "muhammadanser86@gmail.com"
      Env     = upper(terraform.workspace)
      Owner   = "muhammadanser"
      Product = "devops-exercise"
    }
  }

  env = {
    default = {
    }
    devops-exe-dev = {

      location                     = "East US2"
      rg_name                      = "rg-devops-dev-eus2"
      vnet_name                    = "vnet-devops-dev-eus2"
      vnet_address_prefix          = "10.2.0.0/16"
      subnet_common_name           = "subnet-devops-common-dev-eus2"
      subnet_common_address_prefix = "10.2.0.0/24"
      subnet_aks_name              = "subnet-devops-k8svnodes-dev-eus2"
      subnet_aks_address_prefix    = "10.2.2.0/24"
      aks_name                     = "aks-val-dev-eus2"
      aks_dns_prefix               = "aks-val-dev-eus2"
      kubernetes_version           = "1.26.0"
      node_resource_group          = "rg-aksnode-devops-appsvc-dev-eus2"
      default_node_pool_name       = "akspooliedev"
      node_count                   = "4"
      node_max_count               = "8"
      node_min_count               = "4"
      node_vm_size                 = "Standard_DS2_v2"
      node_type                    = "VirtualMachineScaleSets"
      enable_auto_scaling          = true
      load_balancer_sku            = "standard"
      network_plugin               = "azure"
      docker_bridge_cidr           = "172.17.0.1/16"
      dns_service_ip               = "10.2.1.15"
      service_cidr                 = "10.2.1.0/24"
      identity_type                = "SystemAssigned"
    }
    devops-exe-qa = {

    }
  }

  environmentvars = contains(keys(local.env), terraform.workspace) ? terraform.workspace : "default"
  workspace       = merge(local.env["default"], local.env[local.environmentvars])
}
